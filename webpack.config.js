const path = require('path'),
	HtmlWebpackPlugin = require('html-webpack-plugin'),
	CopyPlugin = require('copy-webpack-plugin'),
	MiniCssExtractPlugin = require('mini-css-extract-plugin'),
	autoprefixer = require('autoprefixer'),
	{ CleanWebpackPlugin } = require('clean-webpack-plugin'),
	{ SourceMapDevToolPlugin } = require('webpack'),
	ImageminPlugin = require('imagemin-webpack')

module.exports = {
	entry: './src/index.js',
	output: {
		path: path.join(__dirname, 'dist'),
		filename: 'bundle.[hash].js',
	},
	devServer: {
		contentBase: path.join(__dirname, 'dist'),
		compress: true,
		port: 9000,
		open: true,
	},
	devtool: 'inline-source-map',
	module: {
		rules: [
			{
				test: /\.(js|jsx)$/,
				exclude: /node_modules/,
				use: ['babel-loader', 'eslint-loader', 'source-map-loader'],
			},
			{
				test: /\.(sa|sc|c)ss$/,
				use: [
					MiniCssExtractPlugin.loader,
					'css-loader',
					{
						loader: 'postcss-loader',
						options: {
							autoprefixer: {
								browser: ['last 2 versions'],
							},
							plugins: () => [autoprefixer],
						},
					},
					'sass-loader',
				],
			},
			{
				test: /\.(jpe?g|png|gif|svg)$/i,
				use: [
					{
						loader: 'file-loader',
						options: {
							name: 'assets/media/[hash].[ext]',
						},
					},
				],
			},
			{
				test: /\.(ttf|eot|woff2?|mp3|mp4|txt|pdf|xml)$/i,
				use: 'file-loader?name=data/[name].[ext]',
			},
		],
	},
	resolve: {
		extensions: ['.js', '.jsx'],
	},
	plugins: [
		new CleanWebpackPlugin(),
		new HtmlWebpackPlugin({
			template: './src/index.html',
		}),
		new MiniCssExtractPlugin({
			filename: 'index.[hash].css',
		}),
		new CopyPlugin({
			patterns: [
				{
					from: './src/assets/',
					to: 'assets/',
				},
			],
		}),
		new SourceMapDevToolPlugin({
			filename: '[file].[hash].map',
		}),
		new ImageminPlugin({
			bail: false,
			cache: true,
			imageminOptions: {
				plugins: [
					['gifsicle', { interlaced: true }],
					['jpegtran', { progressive: true }],
					['optipng', { optimizationLevel: 5 }],
					[
						'svgo',
						{
							plugins: [
								{
									removeViewBox: false,
								},
							],
						},
					],
				],
			},
		}),
	],
}
